//
//  BluetoothManager.h
//  MyBeacons
//
//  Created by Joshua O'Neal on 3/25/14.
//  Copyright (c) 2014 Lextech. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <Foundation/Foundation.h>

@class BluetoothManager;

@protocol BluetoothManagerDelegate <NSObject>

@required
- (void)bluetoothManager:(BluetoothManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region;

@end


@interface BluetoothManager : NSObject

@property (nonatomic, weak) id<BluetoothManagerDelegate> delegate;

+ (BluetoothManager *)sharedManager;

/* Where regions is an array of IBeaconRegion */
- (void)startMonitoringRegions:(NSArray *)regions;
- (void)stop;

@end
