//
//  BluetoothManager.m
//  MyBeacons
//
//  Created by Joshua O'Neal on 3/25/14.
//  Copyright (c) 2014 Lextech. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "BluetoothManager.h"
#import "IBeaconRegion+Custom.h"

@interface BluetoothManager () <CLLocationManagerDelegate>

/* This will do the actual scanning for us */
@property (nonatomic, strong) CLLocationManager *locationManager;

/* These are CLBeaconRegion, created from start: data */
@property (nonatomic, strong) NSMutableArray *scannableRegions;

@end

@implementation BluetoothManager

#pragma mark - Overrides

- (id)init
{
    if( self = [super init])
    {
        _scannableRegions = [NSMutableArray array];
        
        _locationManager            = [[CLLocationManager alloc] init];
        _locationManager.delegate   = self;
    }
    
    return self;
}

#pragma mark - Singleton

+ (BluetoothManager *)sharedManager
{
    static BluetoothManager *instance;
    
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        instance = [[BluetoothManager alloc] init];
    });
    
    return instance;
}

#pragma mark - Lifecycle

- (void)startMonitoringRegions:(NSArray *)regions
{
    self.scannableRegions = [NSMutableArray array];

    // store them all internally, so we can stop them later
    for( IBeaconRegion *region in regions )
    {
        [self.scannableRegions addObject:[region asCLBeaconRegion]];
    }
    
    // now start them
    [self.scannableRegions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self.locationManager startMonitoringForRegion:obj];
    }];
}

- (void)stop
{
    // stop monitoring for all regions
    [self.scannableRegions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self.locationManager stopMonitoringForRegion:obj];
        [self.locationManager stopRangingBeaconsInRegion:obj];
    }];
}

#pragma mark - CLLocationManager Failure

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"General failure: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    NSLog(@"Monitoring failure: %@, %@", region, error);
}

- (void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
{
    NSLog(@"Ranging failure: %@, %@", region, error);
}

#pragma mark - CLLocationManager

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
//    NSLog(@"Started monitoring: %@", region);
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    // if we started monitoring already in a region, start ranging
    if( CLRegionStateInside == state )
    {
//        NSLog(@"Inside: %@", region);
        [manager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"Entered: %@", region);
    
    // if we weren't in a region, then entered, start ranging
    [manager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    NSLog(@"Exited: %@", region);
    
    // if we were in a region, then left, stop ranging
    [manager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    // comment this out, unless debugging, gets very noisy (1Hz ranging)
//    NSLog(@"Ranged beacons: %@, %d", region, beacons.count);
    
    if( self.delegate )
    {
        [self.delegate bluetoothManager:self didRangeBeacons:beacons inRegion:region];
    }
}

@end
