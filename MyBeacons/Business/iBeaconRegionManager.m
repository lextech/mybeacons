//
//  iBeaconRegionManager.m
//  MyBeacons
//
//  Created by Joshua O'Neal on 3/25/14.
//  Copyright (c) 2014 Lextech. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "iBeaconRegionManager.h"
#import "AppDelegate.h"
#import "IBeaconRegion+Custom.h"

@interface iBeaconRegionManager ()

@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;

@end

@implementation iBeaconRegionManager

#pragma mark - Properties

- (NSManagedObjectContext *)managedObjectContext
{
    return ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
}

#pragma mark - Singleton

+ (iBeaconRegionManager *)sharedManager
{
    static iBeaconRegionManager *instance;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [[iBeaconRegionManager alloc] init];
    });
    
    return instance;
}

#pragma mark - CRUD

- (IBeaconRegion *)createRegion:(NSString *)uuid identifier:(NSString *)identifier
{
    // make it
    IBeaconRegion *region = [NSEntityDescription insertNewObjectForEntityForName:[IBeaconRegion entityName]
                                                          inManagedObjectContext:self.managedObjectContext];

    region.uuid        = uuid;
    region.identifier  = identifier;
    
    // save it
    NSError *error = nil;
    
    [self.managedObjectContext save:&error];
    
    if( error )
    {
        NSLog(@"Create region error: %@", error);
        return nil;
    }

    return region;
}

- (void)deleteRegion:(IBeaconRegion *)region
{
    [self.managedObjectContext deleteObject:region];
    
    // save it
    NSError *error = nil;
    
    [self.managedObjectContext save:&error];
    
    if( error )
    {
        NSLog(@"Delete region error: %@", error);
    }
}

- (NSArray *)allRegions
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[IBeaconRegion entityName]];
    
    NSError *error = nil;
    
    NSArray *regions = [self.managedObjectContext executeFetchRequest:request
                                                                error:&error];
    
    if( error )
    {
        NSLog(@"Get all regions error: %@", error);
        return @[];
    }
    
    return regions;
}

@end
