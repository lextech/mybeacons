//
//  AddRegionTableViewController.m
//  MyBeacons
//
//  Created by Joshua O'Neal on 3/25/14.
//  Copyright (c) 2014 Lextech. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "AddRegionTableViewController.h"

@interface AddRegionTableViewController ()

@end

@implementation AddRegionTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Validation

- (BOOL)iBeaconDataIsValid
{
    NSString *uuidString = self.uuidTextField.text;
    
    // uuid - blank
    if( [uuidString isEqualToString:@""] )
        return NO;
    
    // uuid - format
    NSError *error = nil;
    
    // http://stackoverflow.com/a/20653030
    NSString *pattern = @"[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    if( error )
        return NO;
    
    NSUInteger matches = [regex numberOfMatchesInString:uuidString
                                                options:kNilOptions
                                                  range:NSMakeRange(0, uuidString.length)];
    
    if( 1 != matches )
        return NO;

    
    // identifier
    if( [self.identifierTextField.text isEqualToString:@""] )
        return NO;
    
    return YES;
}

- (void)showValidationError
{
    [[[UIAlertView alloc] initWithTitle:@"Uh-oh!"
                                message:@"Both UUID and Identifier are required, and UUID must be correctly formatted."
                               delegate:nil
                      cancelButtonTitle:@"Okay"
                      otherButtonTitles:nil] show];
}

#pragma mark - TextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if( textField == self.uuidTextField )
    {
        [self.identifierTextField becomeFirstResponder];
    }
    else
    {
        [self.view endEditing:YES];
    }
    
    return NO;
}

#pragma mark - Actions

- (IBAction)didTapSave:(id)sender
{
    [self.view endEditing:YES];
    
    if( [self iBeaconDataIsValid] )
    {        
        [[iBeaconRegionManager sharedManager] createRegion:self.uuidTextField.text
                                                   identifier:self.identifierTextField.text];
    
        [self performSegueWithIdentifier:@"UnwindToConfigureTableSegue" sender:nil];
    }
    else
    {
        [self showValidationError];
    }
}

@end
