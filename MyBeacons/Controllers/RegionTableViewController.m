//
//  RegionTableViewController.m
//  MyBeacons
//
//  Created by Joshua O'Neal on 3/25/14.
//  Copyright (c) 2014 Lextech. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "RegionTableViewController.h"
#import "IBeaconRegion+Custom.h"

@interface RegionTableViewController ()

@property (nonatomic, strong) NSArray *tableData;

@end

@implementation RegionTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableData = [[iBeaconRegionManager sharedManager] allRegions];
}

#pragma mark - Actions

- (IBAction)didTapDone:(id)sender
{
    NSLog(@"Done");
    
    [self performSegueWithIdentifier:@"UnwindToiBeaconTableSegue"
                              sender:nil];
}

- (IBAction)unwindToConfigureTable:(UIStoryboardSegue *)unwindSegue
{
    NSLog(@"Back, refresh");
    
    self.tableData = [[iBeaconRegionManager sharedManager] allRegions];
    
    [self.tableView reloadData];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConfigureRegionCell"];
    
    [self configureCell:cell withRegion:self.tableData[indexPath.row]];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( UITableViewCellEditingStyleDelete == editingStyle )
    {
        IBeaconRegion *region = self.tableData[indexPath.row];
        
        [[iBeaconRegionManager sharedManager] deleteRegion:region];
        
        self.tableData = [[iBeaconRegionManager sharedManager] allRegions];
        
        [tableView reloadData];
    }
}

#pragma mark - Configuration

- (void)configureCell:(UITableViewCell *)cell withRegion:(IBeaconRegion *)region
{
    cell.textLabel.font = [UIFont systemFontOfSize:13.f];
    
    cell.textLabel.text         = region.uuid;
    cell.detailTextLabel.text   = region.identifier;
}

@end
