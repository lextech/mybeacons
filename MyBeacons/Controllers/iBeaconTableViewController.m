//
//  iBeaconTableViewController.m
//  MyBeacons
//
//  Created by Joshua O'Neal on 3/25/14.
//  Copyright (c) 2014 Lextech. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "iBeaconTableViewController.h"
#import "BluetoothManager.h"
#import "BluetoothHelper.h"
#import "IBeaconRegion+Custom.h"

@interface iBeaconTableViewController () <BluetoothManagerDelegate>

/* An array of IBeaconRegion */
@property (nonatomic, strong) NSArray *tableData;

@end

@implementation iBeaconTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // uuids are long
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[UIFont boldSystemFontOfSize:13.f]];
    
    // we need to know when beacons are ranged
    [BluetoothManager sharedManager].delegate = self;
    
    self.tableData = [[iBeaconRegionManager sharedManager] allRegions];
    
    [self restartBluetooth];
}

#pragma mark - Actions

- (IBAction)didTapRefresh:(id)sender
{
    [self restartBluetooth];
}

- (IBAction)unwindToiBeaconTable:(UIStoryboardSegue *)unwindSegue
{
    // reset data;
    self.tableData = [[iBeaconRegionManager sharedManager] allRegions];
    
    [self restartBluetooth];
}

#pragma mark - Bluetooth

- (void)restartBluetooth
{
    [[BluetoothManager sharedManager] stop];

    // give the appearance of a data dump
    for( IBeaconRegion *region in self.tableData )
    {
        region.rangedBeacons = @[];
    }
    
    [self.tableView reloadData];
    
    // begin monitoring again
    [[BluetoothManager sharedManager] startMonitoringRegions:self.tableData];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // "no beacons" cell
    return self.tableData.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self regionForSection:section].uuid;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger beaconCount = [self regionForSection:section].rangedBeacons.count;
    
    return beaconCount > 0 ? beaconCount : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( 0 == [self regionForSection:indexPath.section].rangedBeacons.count)
        return [tableView dequeueReusableCellWithIdentifier:@"NoBeaconsCell"];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RangedBeaconCell"];
    
    IBeaconRegion *region = [self regionForSection:indexPath.section];
    
    CLBeacon *beacon = region.rangedBeacons[indexPath.row];
    
    [self configureCell:cell withBeacon:beacon];
    
    return cell;
}

#pragma mark - Configuration

- (void)configureCell:(UITableViewCell *)cell withBeacon:(CLBeacon *)beacon
{
    /* TODO - custom subclass */
    cell.textLabel.text       = [NSString stringWithFormat:@"Major: %@, Minor: %@", beacon.major, beacon.minor];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Proximity: %@, %.2fm", [BluetoothHelper stringForProximity:beacon.proximity], beacon.accuracy];
    
    cell.backgroundColor = [BluetoothHelper colorForProximity:beacon.proximity];
}

#pragma mark - Helpers

- (IBeaconRegion *)regionForSection:(NSInteger)section
{
    return (IBeaconRegion *)self.tableData[section];
}

#pragma mark - BluetoothManager

- (void)bluetoothManager:(BluetoothManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    NSString *targetUuid        = region.proximityUUID.UUIDString;
    NSString *targetIdentifier  = region.identifier;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid=%@ AND identifier=%@", targetUuid, targetIdentifier];
    
    NSArray *regions = [self.tableData filteredArrayUsingPredicate:predicate];

    // this shouldn't really happen, but in case it does, let's log it
    if( regions.count < 1 )
    {
        NSLog(@"Table not rendering ranged beacon: %@", targetUuid);
        return;
    }
    
    // technically someone could enter the same uuid and identifier multiple times in the config table
    IBeaconRegion *iBeaconRegion = [regions firstObject];
    
    iBeaconRegion.rangedBeacons = beacons;
    
    [self.tableView reloadData];
}

@end
