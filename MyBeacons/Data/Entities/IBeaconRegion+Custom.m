//
//  iBeaconRegion+Custom.m
//  MyBeacons
//
//  Created by Joshua O'Neal on 3/25/14.
//  Copyright (c) 2014 Lextech. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "IBeaconRegion+Custom.h"

#import <objc/runtime.h>


static char kRangedBeaconsKey;

@implementation IBeaconRegion (Custom)

- (void)setRangedBeacons:(NSArray *)rangedBeacons
{
    objc_setAssociatedObject(self, &kRangedBeaconsKey, rangedBeacons, OBJC_ASSOCIATION_COPY);
}

- (NSArray *)rangedBeacons
{
    return objc_getAssociatedObject(self, &kRangedBeaconsKey) ?: @[];
}

+ (NSString *)entityName
{
    return @"IBeaconRegion";
}

- (CLBeaconRegion *)asCLBeaconRegion
{
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:self.uuid];
    
    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                                identifier:self.identifier];
    
    return region;
}

@end
