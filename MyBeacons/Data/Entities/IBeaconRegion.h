//
//  IBeaconRegion.h
//  MyBeacons
//
//  Created by Joshua O'Neal on 3/28/14.
//  Copyright (c) 2014 Lextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface IBeaconRegion : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * uuid;

@end
