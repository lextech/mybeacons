//
//  IBeaconRegion.m
//  MyBeacons
//
//  Created by Joshua O'Neal on 3/28/14.
//  Copyright (c) 2014 Lextech. All rights reserved.
//

#import "IBeaconRegion.h"


@implementation IBeaconRegion

@dynamic identifier;
@dynamic uuid;

@end
