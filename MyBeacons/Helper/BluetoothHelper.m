//
//  BluetoothHelper.m
//  MyBeacons
//
//  Created by Joshua O'Neal on 3/25/14.
//  Copyright (c) 2014 Lextech. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "BluetoothHelper.h"

@implementation BluetoothHelper

+ (NSString *)stringForProximity:(CLProximity)proximity
{
    switch( proximity )
    {
        case CLProximityImmediate:  return @"Immediate";
        case CLProximityNear:       return @"Near";
        case CLProximityFar:        return @"Far";
        case CLProximityUnknown:    return @"Unknown";
    }
}

+ (UIColor *)colorForProximity:(CLProximity)proximity
{
    switch( proximity )
    {
        case CLProximityImmediate:  return [UIColor greenColor];
        case CLProximityNear:       return [UIColor yellowColor];
        case CLProximityFar:        return [UIColor orangeColor];
        case CLProximityUnknown:    return [UIColor redColor];
    }
}

@end
